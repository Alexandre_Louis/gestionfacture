﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionDesFacture.BDD;
using MySql.Data.MySqlClient;

namespace GestionDesFacture.Dao
{
    class FactureDao
    {

        public List<Facture> GetAllFacture() 
        {
            List<Facture> items = new List<Facture>();
            
            MySqlConnection connection = Dao.Connection;
            
            connection.Open();
            string cmdText = "SELECT * FROM `facture`";
            
            MySqlCommand cmd = new MySqlCommand(cmdText, connection);
            //cmd.Prepare(); //facultatif sauf si requête avec paramètre(s)
            // ajout des paramètres (ici il n'y en a pas)
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Facture item = new Facture
                {
                    id = (int)reader["id"],
                    Date_fact = (DateTime)reader["date_fact"],
                    Numero_or = (string)reader["numero_or"],
                    Num_act = (string)reader["num_act"],
                    Accueil = (string)reader["accueil"],
                    Dat_paie = (DateTime)reader["dat_paie"],
                    Fk_vehicule = (int)reader["fk_vehicule"]
                 };
                items.Add(item);
            }
            reader.Close();
            connection.Close();
            return items;
        }

         
        public List<ViewFacture> GetFacture(int numero)
        {
            List<ViewFacture> items = new List<ViewFacture>();

            MySqlConnection connection = Dao.Connection;

            connection.Open();
            string cmdText = "SELECT * FROM `viewfacture` WHERE facture=@numero";
            MySqlCommand cmd = new MySqlCommand(cmdText, connection);
            cmd.Parameters.Add("@numero", MySqlDbType.Int32);
            cmd.Parameters["@numero"].Value = numero;
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            //cmd.Prepare(); //facultatif sauf si requête avec paramètre(s)
            // ajout des paramètres (ici il n'y en a pas)
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ViewFacture item = new ViewFacture
                {
                    description = (string)reader["description"],
                    prix_ht_actuel = (float)reader["prix_ht_actuel"],
                    type = (BDD.Type)reader["type"],
                    taux = (float)reader["taux"],
                    quantite = (int)reader["quantite"],
                    prix_ht_facture = (float)reader["prix_ht_facture"],
                    remise = (float)reader["remise"]
                };
                items.Add(item);
            }
            reader.Close();
            connection.Close();
            return items;
        }


    }
}
