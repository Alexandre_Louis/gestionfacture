﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace GestionDesFacture.Dao
{
    class Dao
    {
        private static MySqlConnection connection;

        public static MySqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    //création de la chîne de connection
                    MySqlConnectionStringBuilder str = new MySqlConnectionStringBuilder();
                    str.Server = ConfigurationManager.AppSettings.Get("Server");
                    str.Port = Convert.ToUInt32(ConfigurationManager.AppSettings.Get("Port"));
                    str.UserID = ConfigurationManager.AppSettings.Get("UserID");
                    str.Password = ConfigurationManager.AppSettings.Get("Password");
                    str.Database = ConfigurationManager.AppSettings.Get("Database");
                    str.CharacterSet = ConfigurationManager.AppSettings.Get("CharacterSet");

                    connection = new MySqlConnection(str.ToString());

                }
                return connection;
            }
        }

    }
}
