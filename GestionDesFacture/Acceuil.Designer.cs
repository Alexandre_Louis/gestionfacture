﻿namespace GestionDesFacture
{
    partial class Acceuil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnListe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnListe
            // 
            this.btnListe.BackColor = System.Drawing.Color.Transparent;
            this.btnListe.BackgroundImage = global::GestionDesFacture.Properties.Resources.BoutonListe;
            this.btnListe.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnListe.FlatAppearance.BorderSize = 0;
            this.btnListe.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnListe.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnListe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListe.Location = new System.Drawing.Point(273, 317);
            this.btnListe.Margin = new System.Windows.Forms.Padding(0);
            this.btnListe.Name = "btnListe";
            this.btnListe.Size = new System.Drawing.Size(340, 56);
            this.btnListe.TabIndex = 0;
            this.btnListe.UseVisualStyleBackColor = false;
            this.btnListe.Click += new System.EventHandler(this.btnListe_Click);
            // 
            // Acceuil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GestionDesFacture.Properties.Resources.PremierPanelGestionFacture;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1206, 671);
            this.Controls.Add(this.btnListe);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Acceuil";
            this.Text = "Acceuil";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnListe;
    }
}

