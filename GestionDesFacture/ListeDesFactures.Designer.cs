﻿using System;
using System.IO;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace GestionDesFacture
{
    partial class ListeDesFactures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVisualRapid = new System.Windows.Forms.Button();
            this.btnPDF = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnVisualRapid
            // 
            this.btnVisualRapid.BackColor = System.Drawing.Color.Transparent;
            this.btnVisualRapid.BackgroundImage = global::GestionDesFacture.Properties.Resources.BoutonVisuRapid;
            this.btnVisualRapid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnVisualRapid.FlatAppearance.BorderSize = 0;
            this.btnVisualRapid.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnVisualRapid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnVisualRapid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVisualRapid.Location = new System.Drawing.Point(369, 106);
            this.btnVisualRapid.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnVisualRapid.Name = "btnVisualRapid";
            this.btnVisualRapid.Size = new System.Drawing.Size(231, 32);
            this.btnVisualRapid.TabIndex = 0;
            this.btnVisualRapid.UseVisualStyleBackColor = false;
            // 
            // btnPDF
            // 
            this.btnPDF.BackColor = System.Drawing.Color.Transparent;
            this.btnPDF.BackgroundImage = global::GestionDesFacture.Properties.Resources.BoutonExportPDF;
            this.btnPDF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPDF.FlatAppearance.BorderSize = 0;
            this.btnPDF.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPDF.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPDF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPDF.Location = new System.Drawing.Point(369, 150);
            this.btnPDF.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnPDF.Name = "btnPDF";
            this.btnPDF.Size = new System.Drawing.Size(231, 32);
            this.btnPDF.TabIndex = 1;
            this.btnPDF.UseVisualStyleBackColor = false;
            this.btnPDF.Click += new System.EventHandler(this.btnPDF_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(80, 46);
            this.listBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(230, 368);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // ListeDesFactures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GestionDesFacture.Properties.Resources.ListeFacture;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(825, 486);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnPDF);
            this.Controls.Add(this.btnVisualRapid);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ListeDesFactures";
            this.Text = "ListeDesFactures";
            this.Load += new System.EventHandler(this.ListeDesFactures_Load);
            this.ResumeLayout(false);

        }

        private void btnPDF_Click(object sender, EventArgs e)
        {

            using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "PDF file|*.pdf", ValidateNames = true })
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A4.Rotate());
                    try
                    {
                        PdfWriter.GetInstance(doc, new FileStream(sfd.FileName, FileMode.Create));
                        doc.Open();
                        doc.Add(new iTextSharp.text.Paragraph("Bonjour"));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    finally
                    {
                        doc.Close();
                    }
                }
            }

        }

        #endregion

        private System.Windows.Forms.Button btnVisualRapid;
        private System.Windows.Forms.Button btnPDF;
        private System.Windows.Forms.ListBox listBox1;
    }
}