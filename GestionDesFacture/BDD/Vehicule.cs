﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesFacture
{
    class Vehicule
    {
    
    public int id { get; set; }
    public string Immat { get; set; }
    public string Modele { get; set; }
    public DateTime Mec { get; set; }
    public DateTime Livraison { get; set; }
    public int Kms { get; set; }
    public string Gamme { get; set; }
    public string Tvv { get; set; }
    public int Fk_client { get; set; }

}
}
