﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesFacture.BDD
{
    class TVH
    {
        public int id { get; set; }
        public float Tva { get; set; }

        public TVH(int id, float tva)
        {
            this.id = id;
            this.Tva = tva;
        }
    }

    
}
