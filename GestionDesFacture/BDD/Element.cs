﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesFacture.BDD
{
    enum Type { 
        mo, 
        piece }

    class Element
    {
        public int id { get; set; }
        public string Description { get; set; }
        public float Prix_ht_actuel { get; set; }
        public Type Type { get; set; }
        public int Fk_tvh { get; set; }



            public Element(int id, string description, float prix_ht_actuel,Type type,int fk_tvh)
        {
            this.id =id;
            this.Description = description;
            this.Prix_ht_actuel = prix_ht_actuel;
            this.Type = type;
            this.Fk_tvh = fk_tvh;
        }
    

}

}
