﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesFacture.BDD
{    
    class ViewFacture
    {
        public string description { get; set; }
        public float prix_ht_actuel { get; set; }
        public Type type { get; set; }
        public float taux { get; set; }
        public int quantite { get; set; }
        public float prix_ht_facture { get; set; }
        public float remise { get; set; }
    }
}
