﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesFacture
{
    class Client
    {

        public int id { get; set; }
        public string Num_compte { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public int Cp { get; set; }
        public string Ville { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }

        public Client(int id, string num_compte,string nom,string prenom,string adresse,int cp,string ville,string tel,string mobile)
        {
            this.id = id;
            this.Num_compte = num_compte;
            this.Nom = nom;
            this.Prenom = prenom;
            this.Adresse = adresse;
            this.Cp = cp;
            this.Ville = ville;
            this.Tel = tel;
            this.Mobile = mobile;
        }

    }
}
