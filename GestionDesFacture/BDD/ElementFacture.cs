﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesFacture.BDD
{
    class ElementFacture
    {
        public int id { get; set; }
        public int Fk_facture { get; set; }
        public int Fk_element { get; set; }
        public int Quantite { get; set; }
        public float Prix_ht_facture { get; set; }
        public float Remise { get; set; }

        public ElementFacture(int id, int fk_facture, int fk_element, int quantite, float prix_ht_facture, float remise){

            this.id = id;
            this.Fk_facture = fk_facture;
            this.Fk_element = fk_element;
            this.Quantite = quantite;
            this.Prix_ht_facture = prix_ht_facture;
            this.Remise = remise;
        }


}
}
