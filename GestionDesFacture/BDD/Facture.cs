﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionDesFacture.BDD
{
    class Facture
    {

        public int id { get; set; }
        public DateTime Date_fact { get; set; }
        public string Numero_or { get; set; }
        public string  Num_act { get; set; }
        public string Accueil { get; set; }
        public DateTime Dat_paie { get; set; }
        public int Fk_vehicule { get; set; }

    }
}
