﻿using GestionDesFacture.BDD;
using GestionDesFacture.Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionDesFacture
{
    public partial class ListeDesFactures : Form
    {
        FactureDao facture = new FactureDao();

        public ListeDesFactures()
        {
            InitializeComponent();
        }
               

        private void ListeDesFactures_Load(object sender, EventArgs e)
        {
            List<Facture> liste_perso = new List<Facture>(facture.GetAllFacture());

            foreach (Facture perso in liste_perso) 
            {
                listBox1.Items.Add("id: "+perso.id+" numero OR:"+perso.Numero_or);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
