﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GestionDesFacture.Dao;
using GestionDesFacture.BDD;

namespace GestionDesFacture
{
    public partial class Acceuil : Form
    {
        public Acceuil()
        {
            InitializeComponent();
            FactureDao factures = new FactureDao();
            List<Facture> listFactures = new List<Facture>();
            listFactures = factures.GetAllFacture();

            listFactures.ForEach(delegate (Facture e)
            {
                Console.WriteLine(e.ToString());
            });

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnListe_Click(object sender, EventArgs e)
        {
            ListeDesFactures listeDesFactures = new ListeDesFactures();
            listeDesFactures.Show();
        }
    }
}
